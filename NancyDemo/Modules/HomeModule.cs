﻿using Nancy;

namespace NancyDemo
{
	public class HomeModule : NancyModule
	{
		public HomeModule ()
		{
			Get ["/"] = parameters => {
				return "Hello World";
			};
		}
	}
}

